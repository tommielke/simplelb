var path = require('path');

var app = require(path.resolve(__dirname, '../server'));
var ds = app.dataSources.postgres;

const SchemaName = 'api';

// scan data schema for table definitions
ds.discoverModelDefinitions({views: true, schema: SchemaName}, function (err, tables) {
    if (err) throw err;
     
    tables.forEach(function (table) {
        console.log('Table: ' + table.name);
        // build model from table properties
        ds.discoverAndBuildModels(table.name, { schema: SchemaName, relations: true},
            function (err, models) {
                if (err) throw err;
                 
                for (const modelName in models) {
                   // app.model(models[modelName], {dataSource: ds});
                }
            }
        );
    });
})

